package com.fusionconnect;

import javax.persistence.Entity;

@Entity
public class C extends B {
    private String txt = "qwe";

    @Override
    public String toString() {
        return "C{" +
                "txt='" + txt + '\'' +
                '}';
    }
}
