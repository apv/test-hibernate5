package com.fusionconnect;

import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Main {
    // Create an EntityManagerFactory when you start the application.
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("JavaHelps");

    @Test
    public void test() {
        EntityManager entityManager = getEm();
        final A a = new A();
        entityManager.persist(a);
        final C c = (C) a.getB();
        System.out.println(a);
        System.out.println("after creation: " + a.getB());
        closeEm(entityManager);


        entityManager = getEm();
        final A a1 = entityManager.createQuery("select a from A a where a.id = :id"  , A.class)
                .setParameter("id", a.getId())
                .getSingleResult();

        System.out.println("a1:" + a1);
        final B b_proxy = a1.getB();
        System.out.println("b_proxy ID:" + b_proxy.getId());
        System.out.println("b_proxy VERSION:" + b_proxy.getVersion());
        System.out.println("b_proxy:" + b_proxy);


        final A a_by_b = entityManager.createQuery("select a from A a where a.c = :b", A.class)
                .setParameter("b", b_proxy)
                .getSingleResult();


        System.out.println("a_by_b: " + a_by_b);
        closeEm(entityManager);


        System.exit(0);
    }

    private static void closeEm(EntityManager entityManager) {
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private static EntityManager getEm() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        System.out.println(entityManager.isOpen());
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        return entityManager;
    }
}
